@extends('layouts.frontend')


@section('contents')
@foreach ($articles as $article)
<div class="post-preview">
    <a href="{{ route('posts.show', $article->id) }}">
        <h2 class="post-title">{{ $article->title }}</h2>
        <h3 class="post-subtitle">{{ substr($article->contents,0,55) }}</h3>
    </a>
    <p class="post-meta">
        Posted by
        <a href="#!">{{ $article->author->name ?? "Staff" }}</a>
        {{ $article->created_at->diffForHumans() }}
    </p>
    Rating: {{ number_format($article->calculateRatings(),2) }}/5
</div>
<hr />
@endforeach

{!! $articles->links('pagination::bootstrap-4') !!}

@endsection