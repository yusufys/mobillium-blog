@extends('layouts.frontend')
@section('styles')
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('rating-js/themes/fontawesome-stars.css')}}">

@endsection
@section('page-title', $article->title)
@section('contents')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if(session()->has('success'))
<div class="alert alert-success">{{session('success')}}</div>
@endif
<article>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 mx-auto">

                    <div>
                        {!! $article->contents !!}
                    </div>
                    <div class="col-12">
                        Rating: {{ number_format($article->calculateRatings(),2) }}/5
                    </div>
                    <div class="col-12">
                        @if($nextArticle)
                        Next:
                        <br>
                        <a href="{{ route('posts.show', $nextArticle->id) }}">{{$nextArticle->title}}</a>
                        @endif
                        <br>
                        @if($previousArticle)
                        Previous:<br>
                        <a href="{{ route('posts.show', $previousArticle->id) }}">{{$previousArticle->title}}</a>
                        @endif
                    </div>
                    @if($userRating)
                    <select id="rate" name="value" disabled>
                        <option value="1" @if($userRating->value == 1) selected @endif>1</option>
                        <option value="2" @if($userRating->value == 2) selected @endif>2</option>
                        <option value="3" @if($userRating->value == 3) selected @endif>3</option>
                        <option value="4" @if($userRating->value == 4) selected @endif>4</option>
                        <option value="5" @if($userRating->value == 5) selected @endif>5</option>
                    </select>
                    @else
                    <form id="rate-form" action="{{ route('ratings.store') }}" method="post">
                        @csrf

                        <input type="hidden" name="article_id" value="{{ $article->id }}">
                        <select id="rate" name="value">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </form>
                    @endif

                </div>

            </div>
        </div>
    </article>
    <hr />
@endsection

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="{{ asset('rating-js/jquery.barrating.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
      $('#rate').barrating({
        theme: 'fontawesome-stars'
      });
      $("#rate").on('change', function(){
          @auth
          $("#rate-form").submit();
          @else
          window.location.replace('{{route('login')}}');
          @endauth

      });
   });
</script>
@endsection