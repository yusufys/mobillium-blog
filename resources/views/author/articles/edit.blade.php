<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Articles') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <!-- Session Status -->
            @if(session()->has('error'))
            <div class="text-red-800 ">{{ session('error') }}</div>
            @endif

            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <div class="flex flex-col">
                <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                        <form action="{{ route('author.articles.destroy', $article->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <x-button class="ml-4 bg-red-800">
                                {{ __('Delete') }}
                            </x-button>
                        </form>
                        <form action="{{ route('author.articles.update', ['article' => $article->id]) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <div class="mx-2">
                                    <x-label for="title" :value="__('Title')" />
                                    <x-input id="title" class="block mt-1 w-full" type="text" name="title"
                                        :value="old('title', $article->title ?? null)" required autofocus />
                                </div>
                                <div class="mx-2">
                                    <x-label for="slug" :value="__('Slug')" />
                                    <x-input id="slug" class="block mt-1 w-full" type="text" name="slug"
                                        :value="old('slug', $article->slug ?? null)" required autofocus />
                                </div>
                                <div class="mx-2">
                                    <x-label for="contents" :value="__('Contents')" />
                                    <textarea rows="10" cols="10" id="contents"
                                        class=" rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 block mt-1 w-full"
                                        type="text" name="contents" required
                                        autofocus>{{ old('contents', $article->contents ?? null) }}</textarea>
                                </div>
                                <div class="mx-2">
                                    <x-label for="publish_at" :value="__('Publish date')" />
                                    <x-input id="publish_at" class="block mt-1 w-full" type="datetime-local"
                                        name="publish_at"
                                        :value="old('publish_at', $article->publish_at->format('Y-m-d\TH:i') ?? null)"
                                        required autofocus />
                                </div>
                                <div class="mx-2 mt-3">
                                    <x-label for="status" :value="__('Active?')" />
                                   <input type="checkbox" name="status" id="status" @if(old('status', $article->status))  checked @endif>
                                </div>
                                <div class="mt-5 mx-2">
                                    <x-button class="ml-4">
                                        {{ __('Submit') }}
                                    </x-button>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>