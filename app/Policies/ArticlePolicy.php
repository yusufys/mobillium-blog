<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ArticlePolicy
{
    use HandlesAuthorization;
    public function delete(User $user, Article $article)
    {
        if ($article->author->is_admin === 1 && $user->is_admin !== 1) {
            return false;
        }
        if ($article->author->is_moderator === 1 && ($user->is_moderator !== 1 || $user->is_admin !== 1)) {
            return false;
        }

        return $user->id === $article->user_id || $user->is_admin === 1 || $user->is_moderator === 1;
    }

    public function forceDelete(User $user, Article $article)
    {
        if ($article->author->is_admin === 1 && $user->is_admin !== 1) {
            return Response::deny('Access Denied!');
        }
        if ($article->author->is_moderator === 1 && ($user->is_moderator !== 1 || $user->is_admin !== 1)) {
            return Response::deny('Access Denied!');
        }

        return ($user->id === $article->user_id || $user->is_admin === 1 || $user->is_moderator === 1) ? Response::allow() : Response::deny('Access Denied!');
    }
}
