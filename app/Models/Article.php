<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Whtht\PerfectlyCache\Traits\PerfectlyCachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory, PerfectlyCachable;
    protected $fillable = [
        'title',
        'slug',
        'contents',
        'publish_at',
        'status',
        'user_id'
    ];
    protected  $casts = [
        'publish_at' => 'datetime:Y-m-d h:i'
    ];
    public function scopePublished($query)
    {
        return $query->where('publish_at', '<=', now())->where('status', 1);
    }

    public function getIsPublishedAttribute()
    {
        return $this->publish_at <= now();
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function calculateRatings($article = null)
    {
        if (!$article) {
            $article = $this;
        }
        $ratings = $article->ratings()->latest()->get();
        $ratingsCount = ceil($ratings->count() * 0.3);
        $first30Percent = $ratings->take($ratingsCount);
        $first30PercentAvg = $first30Percent->average('value');
        $otherPartsPercentAvg = $ratings->whereNotIn('id', $first30Percent->pluck('id'))->average('value') / 2;
        return ($first30PercentAvg + $otherPartsPercentAvg) / 2;
    }
}
