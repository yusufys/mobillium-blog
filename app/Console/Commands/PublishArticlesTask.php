<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;

class PublishArticlesTask extends Command
{
    protected $signature = 'articles:publish';

    protected $description = 'Publishes any articles that passes its publish date';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $articles = Article::query()->where('publish_at', '<=', now())->whereNull('published_at')->take(50)->get();
        $articles->each(function($item, $key){
            $item->status = 1;
            $item->save();
        });
        exit(0);
    }
}
