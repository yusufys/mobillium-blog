<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\User;
use App\Policies\ArticlePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerGates();
    }

    public function registerGates()
    {
        Gate::define('admin', function (User $user) {
            return $user->is_admin === 1;
        });
        Gate::define('moderator', function (User $user) {
            return $user->is_moderator === 1;
        });
        Gate::define('author', function (User $user) {
            return $user->is_author === 1;
        });
        Gate::define('reader', function (User $user) {
            return $user->is_reader === 1;
        });
    }
}
