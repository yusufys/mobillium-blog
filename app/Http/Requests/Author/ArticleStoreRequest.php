<?php

namespace App\Http\Requests\Author;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ArticleStoreRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('author');
    }
    public function prepareForValidation(){
        $this->merge(['user_id' => Auth::id()]);
        $this->merge(['status' => $this->has('status') ? 1 : 0]);
    }
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => 'required|unique:articles,slug',
            'contents' => 'nullable',
            'publish_at' => 'nullable|date',
            'user_id' => 'required',
            'status' => 'nullable'
        ];
    }
}
