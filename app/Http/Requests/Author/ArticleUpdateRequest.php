<?php

namespace App\Http\Requests\Author;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ArticleUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('author');
    }
    public function prepareForValidation()
    {
        $this->merge(['status' => $this->has('status') ? 1 : 0]);
    }
    public function rules()
    {
        return [
            'title' => 'required',
            'slug' => ['required', Rule::unique('articles', 'slug')->ignore($this->route('article') , 'id')],
            'contents' => 'nullable',
            'publish_at' => 'nullable|date',
            'status' => 'nullable'
        ];
    }
}
