<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Rating;
use Illuminate\Http\Request;

class RatePostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function __invoke(Request $request)
    {
        $request->validate([
            'article_id' => ['exists:articles,id', 'required'],
            'value' => ['required', 'numeric', 'max:5','min:1']
        ], $request->all());

        $article = Article::findOrFail($request->article_id);
        if($article->ratings()->where('article_id', $article->id)->where('user_id', auth()->id())->exists()){
            abort(401, 'Whoops! You Already submitted your rating to this article!');
        }
        $rating = Rating::create([
                'article_id' => $article->id,
                'value' => $request->value,
                'user_id' => auth()->id()
        ]);

        return redirect()->back()->with(['success' => 'Sucess']);
    }
}
