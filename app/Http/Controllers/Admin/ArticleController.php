<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ArticleStoreRequest;
use App\Http\Requests\Admin\ArticleUpdateRequest;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::skipCache()->paginate(5);
        return view('admin.articles.index')->with(['articles' => $articles]);
    }

    public function create()
    {
        return view('admin.articles.create');
    }

    public function store(ArticleStoreRequest $request)
    {
        try {
            $article = Article::create($request->validated());
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error',env('APP_DEBUG') == true ? $th->getMessage() : "An eror has occurred");
        }
        return redirect()->route('articles.edit', $article->id);
    }

    public function show(Article $article)
    {
        return view('admin.articles.edit')->with(['article' => $article]);
    }

    public function edit(Article $article)
    {
        return view('admin.articles.edit')->with(['article' => $article]);
    }

    public function update(ArticleUpdateRequest $request, Article $article)
    {
        try {
            $article->update($request->validated());
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', env('APP_DEBUG') == true ? $th->getMessage() : "An eror has occurred");
        }
        return redirect()->route('articles.edit', $article->id);
    }

    public function destroy(Article $article)
    {
        if (request()->user()->cannot('delete', $article)) {
            abort(403);
        }
        $article->delete();
        return redirect()->route('articles.index')->with('success', 'Deleted Successfully');
    }
}
