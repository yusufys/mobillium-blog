<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $articles = Article::with('author', 'ratings')->published()->latest()->paginate(10);
        return view('articles.index')->with(['articles' => $articles]);
    }
    public function show(Article $article)
    {
        $result['userRating'] = $article->ratings()->where('article_id', $article->id)->where('user_id', auth()->id())->first();
        $result['previousArticle'] = Article::published()->find(Article::where('id', '<', $article->id)->max('id'));
        $result['nextArticle'] = Article::published()->find(Article::where('id', '>', $article->id)->min('id'));
        $result['article'] = $article;
        $article->increment('views');
        return view('articles.show')->with($result);
    }
}
