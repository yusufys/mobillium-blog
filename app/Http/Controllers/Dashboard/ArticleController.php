<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Author\ArticleStoreRequest;
use App\Http\Requests\Author\ArticleUpdateRequest;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::skipCache()->where('user_id', auth()->id())->paginate(5);
        return view('author.articles.index')->with(['articles' => $articles]);
    }

    public function create()
    {
        return view('author.articles.create');
    }

    public function store(ArticleStoreRequest $request)
    {
        try {
            $article = Article::create($request->validated());
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error',env('APP_DEBUG') == true ? $th->getMessage() : "An eror has occurred");
        }
        return redirect()->route('author.articles.edit', $article->id);
    }

    public function show($id)
    {
        $article = Article::skipCache()->where('user_id', auth()->id())->findOrFail($id);
        return view('author.articles.edit')->with(['article' => $article]);
    }

    public function edit($id)
    {
        $article = Article::skipCache()->where('user_id', auth()->id())->findOrFail($id);
        return view('author.articles.edit')->with(['article' => $article]);
    }

    public function update(ArticleUpdateRequest $request, $id)
    {
        $article = Article::skipCache()->where('user_id', auth()->id())->findOrFail($id);
        try {
            $article->update($request->validated());
        } catch (\Throwable $th) {
            report($th);
            return redirect()->back()->with('error', env('APP_DEBUG') == true ? $th->getMessage() : "An eror has occurred");
        }
        return redirect()->route('author.articles.edit', $article->id);
    }

    public function destroy(Article $article)
    {
        if (request()->user()->cannot('delete', $article)) {
            abort(403);
        }
        $article->delete();
        return redirect()->route('author.articles.index')->with('success', 'Deleted Successfully');
    }
}
