<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomDate = now()->addDays(rand(0, 5));
        return [
            'title' => $this->faker->text(55),
            'slug' => $this->faker->slug,
            'contents' => $this->faker->realText(200),
            'publish_at' => now()->addDays(rand(0,10)),
            'status' => 1,
            'user_id' => User::first()->id ?? null
        ];
    }
}
