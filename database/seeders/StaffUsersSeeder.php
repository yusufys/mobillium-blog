<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class StaffUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'is_admin' => 1,
            'name' => 'admin',
            'email' => 'admin@mobillium.com',
            'password' => bcrypt('mobillium'),
        ]);
        User::create([
            'is_author' => 1,
            'name' => 'author',
            'email' => 'writer1@mobillium.com',
            'password' => bcrypt('mobillium'),
        ]);
    }
}
