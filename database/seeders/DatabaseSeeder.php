<?php

namespace Database\Seeders;

use App\Models\Rating;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StaffUsersSeeder::class);
        \App\Models\User::factory(100)->create();
        \App\Models\Article::factory(100)->create();
        foreach (User::get() as  $user) {
            foreach (\App\Models\Article::published()->get() as $article) {
                Rating::create([
                    'article_id' => $article->id,
                    'user_id' => $user->id,
                    'value' => rand(1,5)
                ]);
            }
        }
    }
}
