<?php

namespace Tests\Browser;

use App\Models\Article;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use PHPUnit\Framework\Assert;
use Tests\DuskTestCase;

class HomePageTest extends DuskTestCase
{
    use DatabaseMigrations;
    public function setUp(): void
    {
        $this->appUrl = env('APP_URL');
        parent::setUp();
        $this->artisan('migrate:fresh');
        $this->artisan('db:seed');

    }
    public function testSeeWebsiteTitle()
    {
        $this->browse(function (Browser $browser) {
            $appname = env('APP_NAME');
            $browser->visit('/')
                ->assertSee($appname);
                sleep(1000);
        });
    }
    public function testSeeTenPosts()
    {
        $this->browse(
            function (Browser $browser) {
                $postsElements = $browser->driver->findElements(WebDriverBy::xpath('//*[@class="post-preview"]'));
                $articlesCount = Article::published()->count();
                Assert::assertEquals($articlesCount, count($postsElements));
            }
        );
    }
}
