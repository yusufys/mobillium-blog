<?php

use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RatePostController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard\ArticleController as DashboardArticleController;

Route::get('/', [PostController::class, 'index']);
Route::get('/articles/{article}', [PostController::class, 'show'])->name('posts.show');

Route::post('ratings', RatePostController::class)->name('ratings.store');
Route::prefix('dashboard')->middleware(['auth'])->group(function(){
    Route::get('/', function () {
        return view('author-dashboard');
    })->name('dashboard');
    Route::resource('articles', DashboardArticleController::class)->names('author.articles');
});


Route::prefix('admin')->middleware(['auth'])->group(function(){

    Route::get('/', function () {
        return view('admin-dashboard');
    })->name('admin');

    Route::resource('articles', ArticleController::class);
});



require __DIR__.'/auth.php';
